#!/bin/bash

# Script écrit par Maxime Bombar <bombar@crans.org> à partir d'un script de
# Vincent Le Gallic <legallic@crans.org>.

# Appelé régulièrement dans une crontab, il vérifie que l'on est bien abonné
# à tous ses dossiers mail. Si on ne l'est pas, il effectue les abonnements
# et envoie le diff par mail.

#------------------------------------------------------------------------------


# Sur Zamok, le dossier des mails est ~/Mail
MAILDIR=~/Mail

# Le fichier $MAILDIR/subscriptions est le fichier
# utilisé pour mémoriser les abonnements IMAP.

SUB=$MAILDIR/subscriptions

# S'il n'existe pas, on le crée

test -e $SUB || touch $SUB


# On effectue alors le diff entre ce fichier et
# la liste de tous les dossiers mails présents

# Si le diff est vide, ie que l'on est déjà abonnés
# à tous les dossiers, le script ne fait rien.
# Sinon, il abonne.

# Pour lister tous les dossiers mails présents,
# on utilise la fonction find :

# -maxdepth 1 = On ne veut pas les sous-dossier
# -mindepth 1 = On ne veut pas le dossier courant (.)
# -type d = On veut les dossier (directory) pas les fichiers.
# -regex  = On veut les dossiers qui commencent par un point.

# Et on élimine les trois premiers caractères (.\.) qui ne doivent pas
# apparaître dans le fichier

# Par ailleurs, pour avoir un bon diff, et un bon rendu, on trie par
# ordre alphabétique les résultats.


# On place ce diff dans un fichier temporaire #.tmp que personne ne
# devrait posséder dans ce répertoire.

#-------------------------------------------------------------------------------

cd $MAILDIR

(diff -u <(cat $SUB | sort) <(find . -maxdepth 1 -mindepth 1 -type d -regex "\./\..*" | sort | sed 's/^...//'))>\#\.tmp

if [[ -s \#\.tmp ]]
then
    (find . -maxdepth 1 -mindepth 1 -type d -regex "\./\..*" | sort | sed 's/^...//')> $SUB     
    cat \#\.tmp
    echo
    echo "** APPLIED **"
fi
rm \#\.tmp

if [[ "$1" = *"full"* ]]
then
    echo "**Everything is Ok**"
fi

